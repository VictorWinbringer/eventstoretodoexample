using System;
using Shared.Domain;

namespace TodoModule.Domain.Entities.Events
{
    public sealed class TodoCreated : IDomainEvent
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid EventId => Id;
        public long EventNumber { get; set; }
    }
}
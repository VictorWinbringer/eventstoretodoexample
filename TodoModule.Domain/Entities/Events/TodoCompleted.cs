using System;
using Shared.Domain;

namespace TodoModule.Domain.Entities.Events
{
    public sealed class TodoCompleted: IDomainEvent
    {
        public Guid EventId { get; set; }
        public long EventNumber { get; set; }
    }
}
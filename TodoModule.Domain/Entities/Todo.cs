using System;
using System.Collections.Generic;
using System.Linq;
using Shared.Domain;
using TodoModule.Domain.Entities.Events;

namespace TodoModule.Domain.Entities
{
    public sealed class Todo : IEntity<TodoId>
    {
        private readonly List<IDomainEvent> _events;

        public static Todo CreateFrom(string name)
        {
            var id = Guid.NewGuid();
            var e = new List<IDomainEvent>(){new TodoCreated()
                {
                    Id = id,
                    Name = name
                }};
            return new Todo(new TodoId(id), e, new TodoName(name), false);
        }

        public static Todo CreateFrom(IEnumerable<IDomainEvent> events)
        {
            var id = Guid.Empty;
            var name = String.Empty;
            var completed = false;
            var ordered = events.OrderBy(e => e.EventNumber).ToList();
            if (ordered.Count == 0)
                return null;
            foreach (var @event in ordered)
            {
                switch (@event)
                {
                    case TodoRemoved _:
                        return null;
                    case TodoCreated created:
                        name = created.Name;
                        id = created.Id;
                        break;
                    case TodoCompleted _:
                        completed = true;
                        break;
                    default: break;
                }
            }
            if (id == default)
                return null;
            return new Todo(new TodoId(id), new List<IDomainEvent>(), new TodoName(name), completed);
        }

        private Todo(TodoId id, List<IDomainEvent> events, TodoName name, bool isCompleted)
        {
            Id = id;
            _events = events;
            Name = name;
            IsCompleted = isCompleted;
            Validate();
        }

        public TodoId Id { get; }
        public IReadOnlyList<IDomainEvent> Events => _events;
        public TodoName Name { get; }
        public bool IsCompleted { get; private set; }

        public void Complete()
        {
            if (!IsCompleted)
            {
                IsCompleted = true;
                _events.Add(new TodoCompleted()
                {
                    EventId = Guid.NewGuid()
                });
            }
        }

        public void Delete()
        {
            _events.Add(new TodoRemoved()
            {
                EventId = Guid.NewGuid()
            });
        }

        private void Validate()
        {
            if (Events == null)
                throw new ApplicationException("������ ������ �������");
            if (Name == null)
                throw new ApplicationException("������ �������� ������");
            if (Id == null)
                throw new ApplicationException("������ ������������� ������");
        }
    }
}
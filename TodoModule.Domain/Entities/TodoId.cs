using System;
using Shared.Domain;

namespace TodoModule.Domain.Entities
{
    public class TodoId:NotEmptyStructureBase<Guid>
    {
        public TodoId(Guid value) : base(value)
        {
        }
    }
}
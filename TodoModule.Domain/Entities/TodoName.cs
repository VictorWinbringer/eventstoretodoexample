using Shared.Domain;

namespace TodoModule.Domain.Entities
{
    public class TodoName : NotEmptyStringBase
    {
        public TodoName(string value) : base(value)
        {
        }
    }
}
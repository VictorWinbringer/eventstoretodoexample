using System.Collections.Generic;
using System.Threading.Tasks;
using TodoModule.Domain.Entities;

namespace TodoModule.Domain
{
    public interface ITodoRepository 
    {
        Task SaveAsync(Todo entity);
        Task<Todo> GetAsync(TodoId id);
        Task<List<Todo>> GetAllAsync();
    }
}
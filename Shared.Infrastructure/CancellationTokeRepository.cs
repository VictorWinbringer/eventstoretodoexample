﻿using System.Threading;
using Microsoft.Extensions.Hosting;
using Shared.Domain;

namespace Shared.Infrastructure
{
    public class CancellationTokeRepository : ICancellationTokeRepository
    {
        private readonly IHostApplicationLifetime _lifetime;

        public CancellationTokeRepository(IHostApplicationLifetime lifetime)
        {
            _lifetime = lifetime;
        }
        public CancellationToken Get()
        {
            return _lifetime.ApplicationStopping;
        }
    }
}
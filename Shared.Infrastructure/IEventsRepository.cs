using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shared.Domain;

namespace Shared.Infrastructure
{
    public interface IEventsRepository
    {
        Task<long> Add(Guid collectionId, IEnumerable<IDomainEvent> events);
        Task<List<IDomainEvent>> Get(Guid collectionId);
        Task<List<T>> GetAll<T>() where T : IDomainEvent;
    }
}
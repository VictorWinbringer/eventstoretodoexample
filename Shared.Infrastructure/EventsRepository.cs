using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Shared.Domain;
using TodoModule.Domain.Entities.Events;

namespace Shared.Infrastructure
{
    public sealed class EventsRepository : IEventsRepository
    {
        private readonly IEventStoreConnection _connection;

        public EventsRepository(IEventStoreConnection connection)
        {
            _connection = connection;
        }

        public async Task<long> Add(Guid collectionId, IEnumerable<IDomainEvent> events)
        {
            var eventPayload = events.Select(e => new EventData(
                e.EventId,
                e.GetType().Name,
                true,
                Encoding.UTF8.GetBytes(JsonSerializer.Serialize((object)e)),
                Encoding.UTF8.GetBytes((string)e.GetType().FullName)
            ));
            var res = await _connection.AppendToStreamAsync(collectionId.ToString(), ExpectedVersion.Any, eventPayload);
            return res.NextExpectedVersion;
        }

        public async Task<List<IDomainEvent>> Get(Guid collectionId)
        {
            var results = new List<IDomainEvent>();
            long start = 0L;
            while (true)
            {
                var events = await _connection.ReadStreamEventsForwardAsync(collectionId.ToString(), start, 4096, false);
                if (events.Status != SliceReadStatus.Success)
                    return results;
                results.AddRange(Deserialize(events.Events));
                if (events.IsEndOfStream)
                    return results;
                start = events.NextEventNumber;
            }
        }

        public async Task<List<T>> GetAll<T>() where T : IDomainEvent
        {
            var results = new List<IDomainEvent>();
            Position start = Position.Start;
            while (true)
            {
                var events = await _connection.ReadAllEventsForwardAsync(start, 4096, false);
                results.AddRange(Deserialize(events.Events.Where(e => e.Event.EventType == typeof(T).Name)));
                if (events.IsEndOfStream)
                    return results.OfType<T>().ToList();
                start = events.NextPosition;
            }
        }

        private List<IDomainEvent> Deserialize(IEnumerable<ResolvedEvent> events) =>
            events
                .Where(e => IsEvent(e.Event.EventType))
                .Select(e =>
                {
                    var result = (IDomainEvent)JsonSerializer.Deserialize(e.Event.Data, ToType(e.Event.EventType));
                    result.EventNumber = e.Event.EventNumber;
                    return result;
                })
                .ToList();

        private static bool IsEvent(string eventName)
        {
            return eventName switch
            {
                nameof(TodoCreated) => true,
                nameof(TodoCompleted) => true,
                nameof(TodoRemoved) => true,
                _ => false
            };
        }
        private static Type ToType(string eventName)
        {
            return eventName switch
            {
                nameof(TodoCreated) => typeof(TodoCreated),
                nameof(TodoCompleted) => typeof(TodoCompleted),
                nameof(TodoRemoved) => typeof(TodoRemoved),
                _ => throw new NotImplementedException(eventName)
            };
        }
    }
}
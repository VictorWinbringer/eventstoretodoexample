using System;

namespace EventStoreTodo.Models
{
    public class TodoUpdateModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
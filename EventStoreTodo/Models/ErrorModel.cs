using System.Collections;

namespace EventStoreTodo.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public IDictionary Data { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventStoreTodo.Models;
using Microsoft.AspNetCore.Mvc;
using TodoModule.Domain;
using TodoModule.Domain.Entities;

namespace EventStoreTodo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodosController : ControllerBase
    {
        private readonly ITodoRepository _repository;

        public TodosController(ITodoRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("by-ids")]
        public async Task<List<TodoReadModel>> Get([FromQuery] List<Guid> ids)
        {
            var todos = await Task.WhenAll(ids.Select(i => _repository.GetAsync(new TodoId(i))));
            return todos.Where(t => t != null).Select(t => new TodoReadModel()
            {
                Id = t.Id.Value,
                Name = t.Name.Value,
                IsComplete = t.IsCompleted
            }).ToList();
        }

        [HttpGet]
        public async Task<List<TodoReadModel>> GetAll()
        {
            var todos = await _repository.GetAllAsync();
            return todos.Select(t => new TodoReadModel()
            {
                Id = t.Id.Value,
                Name = t.Name.Value,
                IsComplete = t.IsCompleted
            }).ToList();
        }

        [HttpPost]
        public async Task<Guid> Create(TodoCreateModel dto)
        {
            var todo = Todo.CreateFrom(dto.Name);
            await _repository.SaveAsync(todo);
            return todo.Id.Value;
        }

        [HttpPut("{id}/complete")]
        public async Task Complete(Guid id)
        {
            var todo = await _repository.GetAsync(new TodoId(id));
            todo.Complete();
            await _repository.SaveAsync(todo);
        }

        [HttpDelete("{id}")]
        public async Task Remove(Guid id)
        {
            var todo = await _repository.GetAsync(new TodoId(id));
            todo.Delete();
            await _repository.SaveAsync(todo);
        }
    }
}

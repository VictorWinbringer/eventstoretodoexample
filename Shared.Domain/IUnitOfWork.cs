﻿using System;
using System.Threading.Tasks;

namespace Shared.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        Task CommitAsync();
    }
}
using System;

namespace Shared.Domain
{
    public interface IDomainEvent
    {
        Guid EventId { get; }
        long EventNumber { get; set; }
    }
}
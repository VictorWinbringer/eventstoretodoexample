﻿using System;

namespace Shared.Domain
{
    public abstract class NotEmptyStringBase : NotNullClassBase<string>
    {
        protected NotEmptyStringBase(string value) : base(value)
        {
            Validate();
        }

        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(Value))
                throw new ApplicationException("Пустая строка");
        }
    }
}
﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Shared.Domain
{

    public abstract class SingleValueBase<T> : ValueObjectBase
    {
        protected SingleValueBase(T value)
        {
            Value = value;
        }

        public T Value { get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}

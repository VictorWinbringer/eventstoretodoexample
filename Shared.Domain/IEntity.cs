using System.Collections.Generic;

namespace Shared.Domain
{
    public interface IEntity<T>
    {
        T Id { get; }
        IReadOnlyList<IDomainEvent> Events { get; }
    }
}
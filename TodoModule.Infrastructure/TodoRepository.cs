using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shared.Infrastructure;
using TodoModule.Domain;
using TodoModule.Domain.Entities;
using TodoModule.Domain.Entities.Events;

namespace EventStoreTodo.Infrastructure
{
    public sealed class TodoRepository : ITodoRepository
    {
        private readonly IEventsRepository _eventsRepository;

        public TodoRepository(IEventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }

        public Task SaveAsync(Todo entity) => _eventsRepository.Add(entity.Id.Value, entity.Events);

        public async Task<Todo> GetAsync(TodoId id)
        {
            var events = await _eventsRepository.Get(id.Value);
            return Todo.CreateFrom(events);
        }

        public async Task<List<Todo>> GetAllAsync()
        {
            var events = await _eventsRepository.GetAll<TodoCreated>();
            var res = await Task.WhenAll(events.Where(t => t != null).Where(e => e.Id != default).Select(e => GetAsync(new TodoId(e.Id))));
            return res.Where(t => t != null).ToList();
        }
    }
}